from django.shortcuts import render, get_object_or_404
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from .models import Propierty, Neighborhood
from .forms import PropiertiesFilter
import json
from django.http import HttpResponse


def detail(request, propierty_id):
    propierty = get_object_or_404(Propierty, pk=propierty_id)
    return render(request, 'detail.html', {'propierty': propierty})


def filter(request):
    f = PropiertiesFilter(request.GET, queryset=Propierty.objects.all())
    return render(request, 'filter.html', {'filter': f})

def cities(request):
    query = Neighborhood.objects.all()
    data = {}
    for a in query:
        data[a.name] = a.city.name
    response = HttpResponse(json.dumps(data))
    return response
    
