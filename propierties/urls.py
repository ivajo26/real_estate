# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from .views import *

urlpatterns = patterns('propierties.views',
                       url(r'^detail/(?P<propierty_id>[0-9]+)$', 'detail', name="detail_propierty"),
                       url(r'^barrios/$', 'cities', name="cities"),
                       )
