import django_filters
from .models import Propierty
from django import forms
from .models import Type_Property, City, Neighborhood, TYPE_BUSINESS


class PropiertiesFilter(django_filters.FilterSet):
    id = django_filters.CharFilter(widget=forms.NumberInput(attrs={'placeholder': 'Codigo', 'class': 'selectBox', 'min': 1}))
    type_property = django_filters.ModelChoiceFilter(queryset=Type_Property.objects.all(), widget=forms.Select(attrs={'placeholder': 'Tipo de propiedad'}))
    city = django_filters.ModelChoiceFilter(queryset=City.objects.all(), widget=forms.Select(attrs={'placeholder': 'Ciudad'}))
    price_sell = django_filters.RangeFilter()
    price_rent = django_filters.RangeFilter()
    type_business = django_filters.ChoiceFilter(choices=TYPE_BUSINESS, widget=forms.Select(attrs={'placeholder': 'Tipo de negocio'}))
    neighborhood = django_filters.ModelChoiceFilter(queryset=Neighborhood.objects.all(), widget=forms.Select(attrs={'placeholder': 'Barrio'}))
    class Meta:
        model = Propierty
        fields = {'type_property', 'type_business', 'city', 'price_sell', 'price_rent', 'id', 'neighborhood'}
