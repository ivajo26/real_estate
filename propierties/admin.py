from django.contrib import admin
from .models import Propierty, Image, Contry, Department, City, Type_Property, Neighborhood


class ImagesInline(admin.StackedInline):
    model = Image
    extra = 10


class PropiertyAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'publish']}),
        ('Informacion del propietario', {'fields': ['owner', 'owner_phone'], 'classes': ['collapse']}),
        ('Informacion de Ubicacion', {'fields': ['city', 'neighborhood', 'address'], 'classes': ['collapse']}),
        ('Informacion de la Propiedad', {'fields': ['type_property', 'land_area', 'bedroom', 'bathroom', 'garage', 'stratum', 'floors', 'description'], 'classes': ['collapse']}),
        ('Informacion de Negocio', {'fields': ['type_business', 'price_sell', 'price_rent'], 'classes': ['collapse']}),
    ]
    list_display = ('name', 'id')
    search_fields = ['name', 'id']
    inlines = [ImagesInline]

admin.site.register(Propierty, PropiertyAdmin)
admin.site.register(City)
admin.site.register(Neighborhood)
admin.site.register(Department)
admin.site.register(Contry)
admin.site.register(Type_Property)
