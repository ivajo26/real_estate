# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from smart_selects.db_fields import ChainedForeignKey
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Contry(models.Model):
    name = models.CharField(('Nombre'), max_length=40)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = ('Pais')
        verbose_name_plural = ('Paises')


@python_2_unicode_compatible
class Department(models.Model):
    name = models.CharField(('Nombre'), max_length=40)
    contry = models.ForeignKey(Contry)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = ('Departamento')
        verbose_name_plural = ('Departamentos')


@python_2_unicode_compatible
class City(models.Model):
    name = models.CharField(('Nombre'), max_length=40)
    department = models.ForeignKey(Department)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = ('Ciudad')
        verbose_name_plural = ('Ciudades')


@python_2_unicode_compatible
class Neighborhood(models.Model):
    name = models.CharField(('Nombre'), max_length=40)
    city = models.ForeignKey(City)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = ('Barrio')
        verbose_name_plural = ('Barrios')

    def related_neighborhood(self, obj):
        return obj.neighborhood.name
    related_neighborhood.short_description = 'Barrio'


@python_2_unicode_compatible
class Type_Property(models.Model):
    name = models.CharField(('Nombre'), max_length=40)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = ('Tipo de Propiedad')
        verbose_name_plural = ('Tipo de Propiedades')

TYPE_BUSINESS = (
    ('Venta', 'Venta'),
    ('Arriendo', 'Arriendo'),
    ('Venta-Arriendo', 'Venta-Arriendo')
)


@python_2_unicode_compatible
class Propierty(models.Model):
    name = models.CharField(('Nombre'), max_length=40)
    city = models.ForeignKey(City, verbose_name="Ciudad")
    neighborhood = models.ForeignKey(Neighborhood, blank=True, null=True, on_delete=models.SET_NULL, verbose_name="Barrio")
    address = models.CharField(('Dirección'), max_length=200, null=True)
    # zona foren
    type_property = models.ForeignKey(Type_Property, verbose_name="Tipo de Propiedad")
    type_business = models.CharField(('Tipo de Negocio'), max_length=50, choices=TYPE_BUSINESS)
    land_area = models.DecimalField(('Area'), max_digits=10, decimal_places=2)
    bedroom = models.IntegerField(('Habitaciones'), default=0)
    bathroom = models.IntegerField(('Baños'), default=0)
    garage = models.IntegerField(('Garaje'), default=0)
    stratum = models.IntegerField(('Estrato'), default=0)
    price_sell = models.IntegerField(('Precio de Venta'), default=0)
    price_rent = models.IntegerField(('Precio de Arriendo'), default=0)
    floors = models.IntegerField(('Pisos'), default=0)
    description = models.TextField(('Descripcion'), max_length=1000)
    date_add = models.DateField(default=timezone.now)
    publish = models.BooleanField(('Publicar'), default=False)
    owner = models.CharField(('Propietario'), max_length=100, null=True)
    owner_phone = models.CharField(('Telefono del Propietario'), max_length=100, null=True)

    def __str__(self):
        return self.name

    def thumbnail_url(self):
        images = Image.objects.filter(propierty=self.id).order_by('id')
        if images > 0:
            image = ""
            for img in images:
                return img
                break
            return "images"
        else:
            return "no images"

    def get_images(self):
        images = Image.objects.filter(propierty=self.id).order_by('id')
        return images

    class Meta:
        verbose_name = ('Propiedad')
        verbose_name_plural = ('Propiedades')


class Image(models.Model):
    url = models.ImageField(upload_to='photos/')
    propierty = models.ForeignKey(Propierty)
    thumbnail = ImageSpecField(source='url', processors=[ResizeToFill(355, 178)], format='JPEG', options={'quality': 100})
    thumbnail_detail = ImageSpecField(source='url', processors=[ResizeToFill(182, 100)], format='JPEG', options={'quality': 100})

    class Meta:
        verbose_name = ('Imagen')
        verbose_name_plural = ('Imagenes')
