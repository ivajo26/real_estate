# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Contry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('contry', models.ForeignKey(to='propierties.Contry')),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.ImageField(upload_to=b'photos/')),
            ],
        ),
        migrations.CreateModel(
            name='Propierty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('type_business', models.CharField(max_length=50, choices=[(b'Venta', b'Venta'), (b'Alquiler', b'Alquiler'), (b'Venta-Alquiler', b'Venta-Alquiler')])),
                ('land_area', models.DecimalField(max_digits=10, decimal_places=2)),
                ('bedroom', models.IntegerField(default=0)),
                ('bathroom', models.IntegerField(default=0)),
                ('garage', models.IntegerField(default=0)),
                ('stratum', models.IntegerField(default=0)),
                ('price_sell', models.IntegerField(default=0)),
                ('price_rent', models.IntegerField(default=0)),
                ('floors', models.IntegerField(default=0)),
                ('description', models.TextField(max_length=1000)),
                ('date_add', models.DateField(default=django.utils.timezone.now)),
                ('publish', models.BooleanField(default=False)),
                ('city', models.ForeignKey(to='propierties.City')),
            ],
        ),
        migrations.CreateModel(
            name='Type_Property',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
            ],
        ),
        migrations.AddField(
            model_name='propierty',
            name='type_property',
            field=models.ForeignKey(to='propierties.Type_Property'),
        ),
        migrations.AddField(
            model_name='image',
            name='propierty',
            field=models.ForeignKey(to='propierties.Propierty'),
        ),
        migrations.AddField(
            model_name='city',
            name='department',
            field=models.ForeignKey(to='propierties.Department'),
        ),
    ]
