from django.shortcuts import render
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from propierties.models import Propierty
from propierties.forms import PropiertiesFilter
from .forms import contactForm
from django.core.mail import send_mail
from django.template.loader import render_to_string

def home(request):
    try:
        page = request.GET.get('page', 1)
    except PageNotAnInteger:
        page = 1

    f = PropiertiesFilter(request.GET, Propierty.objects.filter(publish=True).order_by('id'))
    propierties = f
    pages = Paginator(propierties, 10, request=request)
    propierties = pages.page(page)
    return render(request, 'home.html', {'propierties': propierties, 'filter': f})


def sell(request):
    try:
        page = request.GET.get('page', 1)
    except PageNotAnInteger:
        page = 1
    f = PropiertiesFilter(request.GET, Propierty.objects.filter(publish=True, type_business__contains='Venta'))
    propierties = f
    pages = Paginator(propierties, 10, request=request)
    propierties = pages.page(page)
    return render(request, 'sell.html', {'propierties': propierties, 'filter': f})


def rent(request):
    try:
        page = request.GET.get('page', 1)
    except PageNotAnInteger:
        page = 1
    f = PropiertiesFilter(request.GET, Propierty.objects.filter(publish=True, type_business__contains='Arriendo'))
    propierties = f
    pages = Paginator(propierties, 10, request=request)
    propierties = pages.page(page)
    return render(request, 'rent.html', {'propierties': propierties, 'filter': f})


def contact(request):
    if request.method == 'POST':
        form = contactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            email = form.cleaned_data['email']
            recipients = ['encasa-fincaraiz@hotmail.com']
            message_render = render_to_string('mail.html', {'email': email, 'message': message})
            send_mail(subject, message_render, email, recipients)

    form = contactForm()
    return render(request, 'contact.html', {'form': form})


def about(request):
    return render(request, 'aboutus.html')


def requeriments(request):
    return render(request, 'requeriments.html')
