var type_business = $('#id_type_business')
jo = type_business.html();
jo ='<option value="">--</option >'+jo;
type_business.html(jo);

jQuery(document).ready(function($) {

    "use strict";

    // Home Slider

    $('#slider').flexslider({
        animation: "slide"
    });

    // Select Box Replacements

    $('select').selectBox({
        mobile: true,
        menuSpeed: 'fast'
    });

    // Accordions

    $( ".accordion" ).accordion({
        heightStyle: "content",
        collapsible: true
    });

    // Area Range

    $( "#area-range" ).slider({
        range: true,
        min: 0,
        max: 1000,
        values: [ 0, 500 ],
        slide: function( event, ui ) {
            $( "#area" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
        }
    });
    $( "#area" ).val( $( "#area-range" ).slider( "values", 0 ) + " - " + $( "#area-range" ).slider( "values", 1 ) );

    // Price Range
    if ($( "#id_price_sell_0" )[0].value == "") {
      $( "#price_sell_text" ).val( wihtComma(0) +" - " + wihtComma(9000000000));
      $( "#id_price_sell_0" ).val(0);
    }else {
      $( "#price_rent_text" ).val( wihtComma($( "#id_price_sell_0" ).val()) +" - " + wihtComma($( "#id_price_sell_1" ).val()));
    }
    if ($( "#id_price_sell_1" )[0].value == "") {
      $( "#price_sell_text" ).val( wihtComma(0) +" - " + wihtComma(9000000000));
      $( "#id_price_sell_1" ).val(9000000000);
    }

    $( "#price-range-sell" ).slider({
        range: true,
        min: 0,
        max: 9000000000,
        values: [ $( "#id_price_sell_0" )[0].value, $( "#id_price_sell_1" )[0].value ],
        slide: function( event, ui ) {
            $( "#price_sell_text" ).val( wihtComma(ui.values[0]) +" - " + wihtComma(ui.values[1]));
            $( "#id_price_sell_0" ).val(ui.values[0]);
            $( "#id_price_sell_1" ).val(ui.values[1]);
        }
    });

    if ($( "#id_price_rent_0" )[0].value == "") {
      $( "#price_rent_text" ).val( wihtComma(0) +" - " + wihtComma(60000000));
      $( "#id_price_rent_0" ).val(0);
    }else {
      $( "#price_rent_text" ).val( wihtComma($( "#id_price_rent_0" ).val()) +" - " + wihtComma($( "#id_price_rent_1" ).val()));
    }
    if ($( "#id_price_rent_1" )[0].value == "") {
      $( "#price_rent_text" ).val( wihtComma(0) +" - " + wihtComma(60000000));
      $( "#id_price_rent_1" ).val(60000000);
    }else {
      $( "#price_rent_text" ).val( wihtComma($( "#id_price_rent_0" ).val()) +" - " + wihtComma($( "#id_price_rent_1" ).val()));
    }

    $( "#price-range-rent" ).slider({
        range: true,
        min: 0,
        max: 60000000,
        values: [ $( "#id_price_rent_0" )[0].value, $( "#id_price_rent_1" )[0].value ],
        slide: function( event, ui ) {
            $( "#price_rent_text" ).val( wihtComma(ui.values[0]) +" - " + wihtComma(ui.values[1]));
            $( "#id_price_rent_0" ).val(ui.values[0]);
            $( "#id_price_rent_1" ).val(ui.values[1]);
        }
    });


    // Checkbox Replacements

    $('input.checkbox').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%'
    });

    // Gallery Grid

    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        gutter: 0
    });

    // Counter

    $(function() {
        var value;
        $('.counter span').appear();
        $(document.body).on('appear', '.counter span', function(e, $affected) {
            $affected.each(function() {
                value = $(this).data('fact');
                $(this).animateNumbers( value, false, 1000, "easeOutBounce" );
            });
        });
    });

    $('.animated').appear();

});


if (type_business[0].value == 'Venta') {
  $('#price_rent').addClass('desactive');
  $('#price_sell').removeClass('desactive');
  $( "#id_price_rent_0" ).val("");
  $( "#id_price_rent_1" ).val("");
}
if (type_business[0].value == 'Arriendo') {
  $('#price_sell').addClass('desactive');
  $('#price_rent').removeClass('desactive');
  $( "#id_price_sell_0" ).val("");
  $( "#id_price_sell_1" ).val("");
}
if (type_business[0].value == 'Venta-Arriendo') {
  $('#price_sell').removeClass('desactive');
  $('#price_rent').removeClass('desactive');
  $( "#id_price_sell_0" ).val("");
  $( "#id_price_sell_1" ).val("");
}

type_business.change(function() {
    if (this.value == 'Venta') {
      $('#price_rent').addClass('desactive');
      $('#price_sell').removeClass('desactive');
      $( "#id_price_rent_0" ).val("");
      $( "#id_price_rent_1" ).val("");
    }
    if (this.value == 'Arriendo') {
      $('#price_sell').addClass('desactive');
      $('#price_rent').removeClass('desactive');
      $( "#id_price_sell_0" ).val("");
      $( "#id_price_sell_1" ).val("");
    }
    if (this.value == 'Venta-Arriendo') {
      $('#price_sell').removeClass('desactive');
      $('#price_rent').removeClass('desactive');
      $( "#id_price_sell_0" ).val("");
      $( "#id_price_sell_1" ).val("");
    }
});

var options = document.getElementsByTagName('option');
for (var i = 0; i < options.length; i++) {
  if (options[i].value == "") {
    options[i].innerHTML = options[i].parentElement.getAttribute('placeholder')
  }
}

function wihtComma(value) {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function notComma(value){
  return parseInt(value.replace(',',''));
}
