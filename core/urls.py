# -*- coding: utf-8 -*-

from django.conf.urls import include, url, patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .site import home

urlpatterns = patterns('core.site',
                       url(r'^$', 'home', name='home'),
                       url(r'^sell/$', 'sell', name='sell'),
                       url(r'^rent/$', 'rent', name='rent'),
                       url(r'^contact/$', 'contact', name='contact'),
                       url(r'^aboutus/$', 'about', name='aboutus'),
                       url(r'^requeriments/$', 'requeriments', name='requeriments'),
                       )
