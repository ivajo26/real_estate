from django import forms


class contactForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'Email', 'required': ''}))
    subject = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Asunto', 'required': ''}))
    message = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Mensaje', 'required': ''}))
